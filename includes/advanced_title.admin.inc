<?php
// $Id$
/**
 * @file
 * Advanced title admin page callback.
 */

function advanced_title_administer_settings($form, &$form_state) {
  $form['advanced_title'] = array(
    '#title' => t('Apply advanced title'),
    '#type'  => 'fieldset',
  );
  // Node selection
  $node_types   = node_type_get_names();
  $options = array();
  foreach ($node_types as $node_type => $node_type_name) {
    $options[$node_type] = $node_type_name;
  }
  $form['advanced_title']['advanced_title_node_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select the desired node type/s that needs advanced title'),
    '#options'       => $options,
    '#default_value' => variable_get('advanced_title_node_types', array()),
  );
  // Taxonomy selection
  $vocabularies = taxonomy_vocabulary_get_names();
  $options = array();
  foreach ($vocabularies as $vocabulary => $vocabulary_info) {
    $options[$vocabulary] = $vocabulary_info->name;
  }
  $form['advanced_title']['advanced_title_vocabularies'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select the desired vocabulary or taxonomy that needs advanced title'),
    '#options'       => $options,
    '#default_value' => variable_get('advanced_title_vocabularies', array()),
  );
  $form['advanced_title']['title_style_submit'] = array(
    '#type'      => 'submit',
    '#value'     => t('Submit'),
  );
  $form['advanced_title']['title_style_delete'] = array(
    '#type'      => 'link',
    '#title'     => t('Delete first !max advanced title fields marked as "deleted"', array('!max' => ADVANCED_TITLE_MAX_FIELD_BATCH)),
    '#href'      => 'admin/structure/advanced-title/delete/fields',
  );
  $form['#submit'][] = 'advanced_title_administer_settings_submit';
  return system_settings_form($form);
}
 
/**
 * Handle post-validation form submission.
 */
function advanced_title_administer_settings_submit($form, &$form_state) {
  module_load_include('inc', 'advanced_title', 'includes/advanced_title_fields');
  $node_types = node_type_get_names();
  $vocabularies = taxonomy_vocabulary_get_names();
  $deleted    = array();
  $inserted   = array();
  $ins_prefix = '';
  $ins_sufix  = '';
  $del_prefix = '';
  $del_sufix  = '';
  // Node
  foreach ($form_state['values']['advanced_title_node_types'] as $node_type => $value) {
    $instance = advanced_title_get_field_instance('node', $node_type);
    if (empty($value) && !empty($instance)) {
      // Delete advanced title fields
      foreach ($instance as $delete) {
        field_delete_instance($delete);
      }
      $del_prefix = '<p>' . t('Deleted advanced title fields on the following node types:') . '</p><ul>';
      $deleted[] = '<li>' . $node_types[$node_type] . '</li>';
      $del_sufix = '</ul>';
    }
    elseif (!empty($value) && empty($instance)) {
      // Insert new advanced title fields
      if (!advanced_title_undelete_field_instance('node', $node_type)) {
        advanced_title_insert_new_field_instance('node', $node_type);
      }
      $ins_prefix = '<p>' . t('Inserted new advanced title fields on the following node types:') . '</p><ul>';
      $inserted[] = '<li>' . $node_types[$node_type] . '</li>';
      $ins_sufix  = '</ul>';
    }
    if (!empty($inserted)) {
      field_cache_clear();
    }
  }
  $node_msg = $ins_prefix . implode('', $inserted) . $ins_sufix . $del_prefix . implode('', $deleted) . $del_sufix;
  $deleted    = array();
  $inserted   = array();
  $ins_prefix = '';
  $ins_sufix  = '';
  $del_prefix = '';
  $del_sufix  = '';
  // Taxonomy
  foreach ($form_state['values']['advanced_title_vocabularies'] as $vocabulary => $value) {
    $instance = advanced_title_get_field_instance('taxonomy_term', $vocabulary);
    if (empty($value) && !empty($instance)) {
      // Delete advanced title fields
      foreach ($instance as $delete) {
        field_delete_instance($delete);
      }
      $del_prefix = '<p>' . t('Deleted advanced title fields on the following vocabularies:') . '</p><ul>';
      $deleted[] = '<li>' . $vocabularies[$vocabulary]->name . '</li>';
      $del_sufix = '</ul>';
    }
    elseif (!empty($value) && empty($instance)) {
      // Insert new advanced title fields or undelete the advanced title fields marked as deleted
      if (!advanced_title_undelete_field_instance('taxonomy_term', $vocabulary)) {
        advanced_title_insert_new_field_instance('taxonomy_term', $vocabulary);
      }
      $ins_prefix = '<p>' . t('Inserted new advanced title fields on the following vocabularies:') . '</p><ul>';
      $inserted[] = '<li>' . $vocabularies[$vocabulary]->name . '</li>';
      $ins_sufix  = '</ul>';
    }
    if (!empty($inserted)) {
      field_cache_clear();
    }
  }
  $vocabulary_msg = $ins_prefix . implode('', $inserted) . $ins_sufix . $del_prefix . implode('', $deleted) . $del_sufix;
  drupal_set_message(filter_xss_admin("$node_msg$vocabulary_msg"));
}

function advanced_title_delete_fields($form, &$form_state) {
  return confirm_form($form,
    t('Are you sure you want to permanently delete the first 100 fields marked as %del?', array('%del' => 'deleted')),
    'admin/structure/advanced-title',
    t('If you have any content left in all fileds that are marked as deleted, it cannot be recovered anymore. This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );

}

function advanced_title_delete_fields_submit($form, &$form_state) {
  module_load_include('inc', 'advanced_title', 'includes/advanced_title_fields');
  // Purge all field infromation
  $balance = 0;
  $msg = '';
  $node_types = variable_get('advanced_title_node_types', array());
  if (!empty($node_types)) {
    foreach ($node_types as $node_type => $value) {
      if (empty($value)) {
        $balance += advanced_title_delete_permanent_field_data('node', $node_type);
      }
    }
  }
  $vacabularies = variable_get('advanced_title_vocabularies', array());
  if (!empty($vacabularies)) {
    foreach ($vacabularies as $vocabulary => $value) {
      if (empty($value)) {
        $balance += advanced_title_delete_permanent_field_data('taxonomy_term', $vocabulary);
      }
    }
  }
  if ($balance >= ADVANCED_TITLE_MAX_FIELD_BATCH) {
    $balance = ADVANCED_TITLE_MAX_FIELD_BATCH;
    $msg = ' However, there still might left in the database. You may execute delete again for the next batch.';
  }
  drupal_set_message(t('You have deleted @balance advanced title fields permanently .@msg', array('@msg' => $msg, '@balance' => $balance)));
  $form_state['redirect'] = 'admin/structure/advanced-title';
}