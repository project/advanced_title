<?php
// $Id$

/**
 * @file
 * Admin page for Title image.
 */

/**
 * Form elements for the font specific setting.
 *
 * This is refactored to a separate function to avoid poluting the
 * general form function image_captcha_settings_form with some
 * specific logic.
 *
 * @return $form, the font settings specific form elements.
 */
function _image_captcha_settings_form_font_section() {
  // Build a list of  all available fonts.
  $available_fonts = array();

  // List of folders to search through for TrueType fonts.
  $fonts = _image_captcha_get_available_fonts_from_directories();
  // Cache the list of previewable fonts. All the previews are done
  // in separate requests, and we don't want to rescan the filesystem
  // every time, so we cache the result.
  variable_set('image_captcha_fonts_preview_map_cache', $fonts);
  // Put these fonts with preview image in the list
  foreach ($fonts as $token => $font) {
    $img_src = check_url(url('admin/config/people/captcha/image_captcha/font_preview/' . $token));
    $title = t('Font preview of @font (@file)', array('@font' => $font->name, '@file' => $font->uri));
    $available_fonts[$font->uri] = '<img src="' . $img_src . '" alt="' . $title . '" title="' . $title . '" />';
  }

  // Append the PHP built-in font at the end.
  $img_src = check_url(url('admin/config/people/captcha/image_captcha/font_preview/BUILTIN'));
  $title = t('Preview of built-in font');
  $available_fonts['BUILTIN'] = t('PHP built-in font: !font_preview',
    array('!font_preview' => '<img src="' . $img_src . '" alt="' . $title . '" title="' . $title . '" />')
  );

  $default_fonts = _image_captcha_get_enabled_fonts();
  $form['image_captcha_fonts'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Fonts'),
    '#default_value' => $default_fonts,
    '#description' => t('Select the fonts to use for the text in the image CAPTCHA. Apart from the provided defaults, you can also use your own TrueType fonts (filename extension .ttf) by putting them in %fonts_library_general or %fonts_library_specific.',
      array(
        '%fonts_library_general' => 'sites/all/libraries/fonts',
        '%fonts_library_specific' => conf_path() . '/libraries/fonts',
      )
    ),
    '#options' => $available_fonts,
    '#attributes' => array('class' => array('image_captcha_admin_fonts_selection')),
    '#process' => array('form_process_checkboxes'),
  );
  return $form;
}

/**
 * Helper function to get fonts from the given directories.
 *
 * @param $directories (optional) an array of directories
 *   to recursively search through, if not given, the default
 *   directories will be used.
 *
 * @return an array of fonts file objects (with fields 'name',
 *   'basename' and 'filename'), keyed on the md5 hash of the font
 *   path (to have an easy token that can be used in an url
 *   without en/decoding issues).
 */
function _title_image_get_available_fonts_from_directories($directories = NULL) {
  // If no fonts directories are given: use the default.
  if ($directories === NULL) {
    $directories = array(
      drupal_get_path('module', 'image_captcha') . '/fonts',
      'sites/all/libraries/fonts',
      conf_path() . '/libraries/fonts',
    );
  }
  // Collect the font information.
  $fonts = array();
  foreach ($directories as $directory) {
    foreach (file_scan_directory($directory, '/\.[tT][tT][fF]$/') as $filename => $font) {
      $fonts[md5($filename)] = $font;
    }
  }

  return $fonts;
}