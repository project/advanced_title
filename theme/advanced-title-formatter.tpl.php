<?php
// $Id$

/**
 * @file advanced-title-formatter.tpl.php
 *
 * Available variable:
 * - $title: can be an array (for multiple titles) or string.
 * - $title_image: rendered title's image logo.
 * - $entity: entity of this content (node, taxonomy_term, user).
 * - $bundle: bundle of this content (book, page, user).
 */

// Smart Paging title suffix support
$smart_paging_title_suffix = theme('smart_paging_page_title_suffix');
// Target head title (<title> tag)
drupal_set_title(drupal_get_title() . $smart_paging_title_suffix);
?>
<?php if (!empty($title)): ?>
  <?php if (is_array($title)): ?>
    <?php $title_count = count($title) - 1; ?>
    <div id="page-title" class="decor-banderitas">
    <?php foreach ($title as $head_index => $title_value): ?>                                            
      <?php if ($head_index == 0): ?>
        <h1 class="text-content-title title"><?php print $title_image; ?><?php print $title_value; ?></h1>
      <?php elseif ($head_index == 1): ?>
        <h2 class="text-content-title title"><?php print $title_value . ($title_count == $head_index ? $smart_paging_title_suffix : ''); ?></h2>
      <?php elseif ($head_index >= 2): ?>
        <h3 class="text-content-title title"><?php print $title_value . ($title_count == $head_index ? $smart_paging_title_suffix : ''); ?></h3>
      <?php endif; ?>
    <?php endforeach; ?>
    </div> <!-- /#site-title -->
  <?php else: ?>
    <h1 id="page-title" class="text-content-title title decor-banderitas"><?php print $title . $smart_paging_title_suffix; ?></h1>
  <?php endif; ?>
<?php endif; ?>