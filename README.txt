$Id$

Description:
Provides multiple title field, image title logo and field for entering CSS class of title 
displayed as menu item.

Requirements:
Drupal 7.x
Title image
Textimage

Installation:
1. Copy the extracted advanced_title directory to your Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module at the http://www.example.com/?q=admin/modules 
as well as Title image and Textimage modules.
3. Configure Advanced title at http://www.example.com/?q=admin/structure/advanced-title
4. Open your theme's page.tpl.php with any text editor and make sure that the $title is coded
as plain print. Usually $title is coded as:
<?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
Change it to:
<?php print $title; ?>
5. Optionally your title can now be themed via advanced-title-formatter.tpl.php

Support:
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/advanced_title